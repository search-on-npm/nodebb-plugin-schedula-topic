"use strict";

var converter = {};
var user = require.main.require('./src/user');
var db = require.main.require('./database'),
	cron = require('node-cron'),
	controllers = require('./lib/controllers'),
	async = require.main.require('async'),
	winston = require.main.require('winston'),
	Topics = require.main.require('./topics'),
	notifications = require.main.require('./notifications'),
	sockets = require('./lib/sockets'),
	SocketPlugins = require.main.require('./src/socket.io/plugins'),
	websockets = module.parent.parent.require('./socket.io');
//library = require("../nodebb-plugin-connect-hashtag/library.js"),


SocketPlugins.connectschedulatopic = sockets;

var hash = "connect-schedula-topic";
var allTimer = {};

converter.init = function(params, callback) {

	var router = params.router,
		hostMiddleware = params.middleware,
		hostControllers = params.controllers;

	router.get('/admin/plugins/connect-schedula-topic', hostMiddleware.admin.buildHeader, controllers.renderAdmin);
	router.get('/api/admin/plugins/connect-schedula-topic', controllers.renderAdmin);



	callback();
};

converter.addAdminNavigation = function(header, callback) {

	header.plugins.push({
		route: '/plugins/connect-schedula-topic',
		icon: 'fa-file-image-o',
		name: 'Connect Schedula Topic (Connect)'
	});

	callback(null, header);
};
//Crea il topic schedulato
converter.modificaTopic = function(data, callback) {

	var timestamp_schedulato = data.data.tempoSchedulazione;
	var timestamp_corrente = Date.now();
	//Caso in cui non schedula il topic
	if (!timestamp_schedulato || timestamp_schedulato == 0) {
		return callback(null, data);
	}
	if (timestamp_schedulato < timestamp_corrente) {

		return callback(null, data);
	}

	var date = new Date(Number(timestamp_schedulato));

	var mese = date.getMonth() + 1; // getMonth() is zero-based
	var giorno = date.getDate();
	var minuti = date.getMinutes();
	var ore = date.getHours();
	var anno = date.getFullYear();
	var uid = data.topic.uid;
	var tid = data.topic.tid;
	async.waterfall([
		function(next) {
			websockets.in('uid_' + data.topic.uid).emit('connectschedulatopic', {

			});
			next();
		},
		function(next) {
			db.sortedSetAdd(hash + ":timer", Date.now(), tid, next);
		},
		function(next) {
			db.setObject(hash + ":timer:tid:" + tid, {
				'timestamp': timestamp_schedulato

			}, next);
		},
		function(next) {
			db.setObject(hash + ":uid:" + uid + ":" + tid, {
				timestamp: timestamp_schedulato
			}, next);
		},
		function(next) {
			db.sortedSetAdd(hash + ":uid:" + uid, Date.now(), tid, next);
		},
		function(next) {
			data.topic.timestamp = timestamp_schedulato;
			data.data.timestamp = timestamp_schedulato;
			db.setObject(hash + ":topic_schedulato:" + tid, {
				topic_schedulato: data.topic
			}, next);
		},
		function(next) {
			db.setObject(hash + ":funzione_dati:" + tid, {
				parametri: data
			}, next);
		}


	], function(err) {
		if (err) {
			return callback(err);
		}
		var stringa = '0 ' + minuti + ' ' + (ore) + ' ' + giorno + ' ' + mese + ' *';

		var task = cron.schedule(stringa, function() {
			async.waterfall([

				function(next) {
					db.sortedSetRemove(hash + ":timer", tid, next);
				},
				function(next) {
					db.delete(hash + ":timer:tid:" + tid, next);
				},
				function(next) {
					db.delete(hash + ":uid:" + uid + ":" + tid, next);
				},
				function(next) {
					db.sortedSetRemove(hash + ":uid:" + uid, tid, next);
				},
				function(next) {
					db.delete(hash + ":topic_schedulato:" + tid, next);
				},
				function(next) {
					db.delete(hash + ":funzione_dati:" + tid, next);
				},
				function(next) {
					var param = {
						titolo: data.topic.title,
						indirizzo: data.topic.slug
					};
					websockets.in('uid_' + uid).emit('connectschedulatopic_schedulato', {
						parametri: param
					});
					next();
				}

			], function(err) {
				if (err) {
					return callback(err);
				}
				task.destroy();
				delete allTimer[tid];
				return callback(null, data);
			});

		}, false);

		allTimer[tid] = {
			task: task,
			funzione: callback,
			parametri: data
		};
		task.start();


	});

};
//Se il server è down o si riavvia bisogna recuperare 
//i topic schedulati
converter.caricaTopicSchedulati = function(data, callback) {
	var uid;
	var topicData;
	async.waterfall([
		function(next) {
			db.getSortedSetRange(hash + ":timer", 0, -1, next);
		},
		function(lista_topic_schedulati, next) {
			if (!lista_topic_schedulati || lista_topic_schedulati.length == 0) {
				return callback(null, data);
			}

			async.each(lista_topic_schedulati, function(tid, avanti) {
				var uid;
				var topicData;
				async.waterfall([
					function(prossima) {
						db.getObject(hash + ":funzione_dati:" + tid, prossima);
					},
					function(topicSchedulato, prossima) {
						if (!topicSchedulato) {
							return avanti();
						}
						topicData = topicSchedulato.parametri.data;
						uid = topicSchedulato.parametri.topic.uid;
						db.sortedSetRemove(hash + ":timer", tid, prossima);

					},

					function(prossima) {
						db.delete(hash + ":timer:tid:" + tid, prossima);
					},
					function(prossima) {
						db.delete(hash + ":uid:" + uid + ":" + tid, prossima);
					},
					function(prossima) {
						db.sortedSetRemove(hash + ":uid:" + uid, tid, prossima);
					},
					function(prossima) {
						db.delete(hash + ":topic_schedulato:" + tid, prossima);
					},
					function(prossima) {
						db.delete(hash + ":funzione_dati:" + tid, prossima);
					},
					function(prossima) {
						var tempoSchedulazione = topicData.tempoSchedulazione;
						topicData.timestamp = tempoSchedulazione;
						Topics.post(topicData, prossima);
					}

				], function(err) {
					if (err) {
						return callback(err);
					}

					return avanti();
				});
			}, function(err) {
				if (err) {
					return callback(err);
				}
				return next();

			});
		}
	], function(err) {
		if (err) {
			return callback(err);
		}

		return callback(null, data);
	});

};

exports.allTimer = allTimer;



module.exports = converter;