"use strict";


(function() {
	//Crea il bottone "Mostra topic schedulati" nel profilo
	$(window).on('action:ajaxify.end', function(event, data) {
		if (new RegExp('user\/' + app.user.username + "$").test(data.url)) {
			$(document).ready(function() {
				socket.emit('plugins.connectschedulatopic.getTopicsSchedulati', {
					'uid': app.user.uid
				}, function(err, topic_schedulati) {
					if (topic_schedulati && topic_schedulati.length != 0) {
						$('div[class="text-center profile-meta"]').append('<br/><div style="text-align: center ;"><br/><button type="button" id="topic_schedulati" class="btn btn-info">Mostra Topics Schedulati</button><br/><br/><div>');
					}
				});

			});
		}

	});


}());