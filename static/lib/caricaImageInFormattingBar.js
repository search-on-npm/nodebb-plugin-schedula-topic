"use strict";
//Carica il calendar nella barra di formattazione

(function() {
	$(document).ready(function() {

		socket.emit('plugins.connectschedulatopic.utenteAppartieneGruppi', {
			'uid': app.user.uid
		}, function(err, gruppi) {
			if (gruppi) {
				var tid = null;
				require(['composer', 'composer/controls'], function(composer, controls) {
					composer.addButton('fa fa-calendar', function(textarea, selectionStart, selectionEnd) {
						if (selectionStart === selectionEnd) {
							tid = ajaxify.data.tid;

							ajaxify.loadTemplate('modals/connect-schedula-topic', function(dataTopicRilievo) {

								var modal = bootbox.dialog({
									message: dataTopicRilievo,
									title: '',
									buttons: {
										success: {
											label: "Schedula",
											className: "btn-primary save",
											callback: function() {
												var date = $('#dataCreazione').val();
												if (!date) {
													app.alertError("Inserire Data e Ora");
													return false;
												}

												date = new Date(date);
												var timestamp = (date.getTime());
												if (timestamp < Date.now()) {
													app.alertError("Selezionare una data futura");
													return false;
												}
												$('#tempo_schedulato').remove();
												$('[component="composer"]').append("<p id='tempo_schedulato' hidden>" + timestamp + "</p>");


											}
										}
									}
								});
								modal.on('shown.bs.modal', function() {
									socket.emit('plugins.connectschedulatopic.getTopicSchedulato', {
										'tid': tid
									}, function(err, date) {
										if (date) {
											var timestamp = date.timestamp;
											var date_finale = formattaData(timestamp);
											$('#dataCreazione').val(date_finale);
										} else {
											var timestampInserito = $('#tempo_schedulato').text();
											if (!timestampInserito) {
												$('#dataCreazione').val(formattaData(Date.now()));
											} else {
												var timestamp = timestampInserito;
												var date_finale = formattaData(timestamp);
												$('#dataCreazione').val(date_finale);


											}
										}
									});

								});


							});

						}
					}, "Schedula Topic");



				});

			}
		});

		function formattaData(timestamp) {
			var date = new Date(Number(timestamp));

			// Minutes part from the timestamp
			var mm = date.getMonth() + 1; // getMonth() is zero-based
			var dd = date.getDate();
			var minutes = date.getMinutes();
			var hh = date.getHours();
			var mese = "";
			var day = "";
			var ore = "";
			var min = "";

			if (minutes < 10) {
				min = "0" + minutes;
			} else {
				min = "" + minutes;
			}
			if (mm < 10) {
				mese = "0" + mm;
			} else {
				mese = "" + mm;
			}
			if (dd < 10) {
				day = "0" + dd;
			} else {
				day = "" + dd;
			}
			if (hh < 10) {
				ore = "0" + hh;
			} else {
				ore = "" + hh;
			}
			var formattedTime = ore + ':' + min;
			var data = date.getFullYear() + "-" + mese + "-" + day;
			return data + "T" + formattedTime;
		}


		//	}



		//});
	});
}());