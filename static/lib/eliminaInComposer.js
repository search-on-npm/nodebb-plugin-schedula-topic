"use strict";


(function() {
	var timeout_alert = 2500;
	//Quando schedula un topic
	socket.on('connectschedulatopic', function(params) {
		//$('div[class="write-container"]').children('textarea').text('');
		$('[component="composer"]').remove();

		app.alert({
			title: 'TOPIC SCHEDULATO',
			message: 'Puoi gestire i tuoi topic schedulati nel tuo profilo',
			location: 'left-bottom',
			timeout: 3500,
			type: 'success'
		});

	});
	//Quando finisce di essere schedulato
	socket.on('connectschedulatopic_schedulato', function(params) {
		app.alert({
			title: 'TOPIC PUBBLICATO',
			message: 'Il Topic è stato corretamente pubblicato',
			location: 'left-bottom',
			timeout: timeout_alert,
			type: 'success'
		});

	});
	//Quando il topic è schedulato ma l'utente lo pubblica
	socket.on('connectschedulatopic_pubblica', function(params) {
		app.alert({
			title: 'TOPIC PUBBLICATO',
			message: 'Il Topic è stato corretamente pubblicato',
			location: 'left-bottom',
			timeout: timeout_alert,
			type: 'success'
		});

	});



	$(window).on('action:composer.submit', function(ev, data) {
		// data.composerEl is the dom element for the composer
		// data.action can be `topics.post`, `posts.reply` or `posts.edit`
		// data.composerData is the object that will be submitted to the server.

		// lets add our new field into the data to be submitted for new topics
		var timestamp = data.composerEl.find('[id="tempo_schedulato"]').text();
		//console.log("data "+JSON.stringify(data));
		if (data.action === 'topics.post') {

			data.composerData.tempoSchedulazione = timestamp;
		}

	});



}());