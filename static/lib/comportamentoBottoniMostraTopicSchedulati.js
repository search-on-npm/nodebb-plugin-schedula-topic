"use strict";

(function() {
	//Gestisce quando si clicca sul pulsante pubblica
	$(document).on('click', 'button[id^="pubblica"]', function() {
		var result = confirm("Vuoi pubblicare adesso il topic?");
		if (result == true) {
			var topic_id = $(this).attr('id').replace('pubblica', '');
			socket.emit('plugins.connectschedulatopic.eliminaTopicSchedulato', {
				'tid': topic_id,
				'uid': app.user.uid
			}, function(err) {
				$('#riga' + topic_id).remove();
				var lunghezza_topic = $('[id^="riga"]').length;
				if (lunghezza_topic == 0) {
					$('#label_scritte').remove();
					$('#schedulati').append("<label>Non ci sono più topic schedulati</label>");
					$('#topic_schedulati').remove();
				}

			});
		}

	});
	//Quando sono in questo metodo sicuramente il topic da schedulare
	//è stato creato
	$(document).on('click', 'button[id^="modifica"]', function() { //app.newTopic();
		var tid = $(this).attr('id').replace('modifica', '');
		ajaxify.loadTemplate('modals/connect-schedula-topic', function(dataTopicRilievo) {

			var modal = bootbox.dialog({
				message: dataTopicRilievo,
				title: '',
				buttons: {
					success: {
						label: "Schedula",
						className: "btn-primary save",
						callback: function() {
							var date = $('#dataCreazione').val();
							if (!date) {
								app.alertError("Inserire Data e Ora");
								return false;
							}

							date = new Date(date);
							var timestamp = (date.getTime());
							if (timestamp < Date.now()) {
								app.alertError("Selezionare una data futura");
								return false;
							}
							socket.emit('plugins.connectschedulatopic.getTopicSchedulato', {
								'tid': tid
							}, function(err, topicSchedulato) {
								//$("body").append("<h1>CIAOOO</h1>");
								socket.emit('plugins.connectschedulatopic.salvaUtenteTimestamp', {
									'uid': app.user.uid,
									'timestamp': timestamp,
									'tid': tid
								}, function(err) {
									/*socket.emit('connectschedulatopic',{timestamp:timestamp},function(err){
										console.log("INVIO SOCKET");
									});*/

									var date_finale = formattaDataInSchedula(timestamp);
									$('#schedula' + tid).text(date_finale);
									$('[component="composer"]').append("<p id='tempo_schedulato' hidden>" + timestamp + "</p>");

									/*app.alert({
										title: 'TOPIC SCHEDULATO',
										message: 'uova data di schedulazione salvata: <b>' + date_finale + '</b>',
										location: 'left-bottom',
										timeout: 4000,
										type: 'success'
									});*/

								});
							});

						}
					}
				}
			});
			modal.on('shown.bs.modal', function() {
				socket.emit('plugins.connectschedulatopic.getTopicSchedulato', {
					'tid': tid
				}, function(err, date) {
					if (date) {
						var timestamp = date.timestamp;
						var date_finale = formattaData(timestamp);
						$('#dataCreazione').val(String(date_finale));
						//$('#schedula'+tid).val(data_finale);
					} else {
						$('#dataCreazione').text(formattaData(Date.now()));
					}
				});

			});



		});


	});

	function formattaData(timestamp) {
		var date = new Date(Number(timestamp));

		// Minutes part from the timestamp
		var mm = date.getMonth() + 1; // getMonth() is zero-based
		var dd = date.getDate();
		var minutes = date.getMinutes();
		var hh = date.getHours();
		var mese = "";
		var day = "";
		var ore = "";
		var min = "";

		if (minutes < 10) {
			min = "0" + minutes;
		} else {
			min = "" + minutes;
		}
		if (mm < 10) {
			mese = "0" + mm;
		} else {
			mese = "" + mm;
		}
		if (dd < 10) {
			day = "0" + dd;
		} else {
			day = "" + dd;
		}
		if (hh < 10) {
			ore = "0" + hh;
		} else {
			ore = "" + hh;
		}


		var formattedTime = ore + ':' + min;
		var data = date.getFullYear() + "-" + mese + "-" + day;
		return data + "T" + formattedTime;
	}

	function formattaDataInSchedula(timestamp) {
		var date = new Date(Number(timestamp));

		// Minutes part from the timestamp
		var mm = date.getMonth() + 1; // getMonth() is zero-based
		var dd = date.getDate();
		var minutes = date.getMinutes();
		var hh = date.getHours();
		var mese = "";
		var day = "";
		var ore = "";
		var min = "";

		if (minutes < 10) {
			min = "0" + minutes;
		} else {
			min = "" + minutes;
		}
		//console.log("IN FORMATTED DATE MESE "+mm+"Giorno "+dd+" hh "+hh+"minute "+minutes);
		if (mm < 10) {
			mese = "0" + mm;
		} else {
			mese = "" + mm;
		}
		if (dd < 10) {
			day = "0" + dd;
		} else {
			day = "" + dd;
		}
		if (hh < 10) {
			ore = "0" + hh;
		} else {
			ore = "" + hh;
		}


		var formattedTime = ore + ':' + min;
		var data = day + "/" + mese + "/" + date.getFullYear();

		return data + " " + formattedTime;
	}



}());