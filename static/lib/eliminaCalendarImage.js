"use strict";


(function() {

	$(window).on('action:composer.loaded', function(err, data) {

		var title_tid = data.composerData.title;



		if (new RegExp('topic\/[0-9]+\/').test(location.href)) {

			var pid = data.composerData.pid;
			socket.emit('plugins.connectschedulatopic.getTopicPid', {
				'pid': pid
			}, function(err, topic) {
				if (topic) {
					if (Date.now() > topic.timestamp) {

						$('li[data-format="calendar"]').remove();
					}
				}

			});

		} else {
			var testo = $('div[class="write-container"]').children('textarea').val();
			if (testo) {
				$('div[class="write-container"]').children('textarea').val("");
			}

		}


	});



}());