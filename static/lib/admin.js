'use strict';
/* globals $, app, socket, Sortable */

define('admin/plugins/connect-schedula-topic', [
	'settings'
], function(colorpicker) {


	var ACP = {};
	ACP.init = function() {

		$('#item_save').on('click', function() {
			var select_name = [];

			$('input[type="checkbox"]:checked').each(function() {
				select_name.push($(this).attr('id'));
			});


			if (!select_name || select_name.length == 0) {
				app.alertError("Selezionare almeno un gruppo");
			} else {
				socket.emit('plugins.connectschedulatopic.inserireGruppi', {
					'gruppi': select_name
				}, function(err, ritorno) {
					app.alertSuccess("Salvataggio avvenuto con successo");

				});
			}

		});
	}
	return ACP;
});