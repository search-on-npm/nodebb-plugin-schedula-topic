"use strict";

//Mostra i topic schedulati
(function() {
	$(document).ready(function() {
		$('body').on('click', '#topic_schedulati', function() {
			socket.emit('plugins.connectschedulatopic.getTopicsSchedulati', {
				'uid': app.user.uid
			}, function(err, topics) {
				if (topics && topics.length != 0) {
					ajaxify.loadTemplate('modals/mostra-topics-schedulati', function(dataTopicRilievo) {

						var modal = bootbox.dialog({
							message: dataTopicRilievo,
							title: 'Topics Schedulati',

						});
						modal.on('shown.bs.modal', function() {
							//console.log("TOPICS " + JSON.stringify(topics.length));
							if (!topics || topics.length == 0) {

								$('#label_scritte').remove();
								$('#schedulati').append("<label>Non ci sono più topic schedulati</label>");
							} else {

								for (var i = 0; i < topics.length; i++) {
									var data = formattaData(topics[i].timestamp);
									//console.log("DATA --------" + JSON.stringify(data) + " TIMESTAMP " + topics[i].timestamp);
									$('#schedulati').append('<div class="row" id="riga' + topics[i].tid + '"><div class="col-xs-12 col-md-12"><div class="col-xs-3 col-md-3 scritte" style="word-wrap: break-word;">' + topics[i].title + '</div><div class="col-xs-3 col-md-3 scritte" style="word-wrap: break-word;" id="schedula'+topics[i].tid+'">' + data + '</div><div class="col-xs-3 col-md-3"><button type="button" id="modifica' + topics[i].tid + '" class="btn btn-success btn-xs riduciBottoneCellulare">Modifica</button></div><div class="col-xs-3 col-md-3"><button type="button" id="pubblica' + topics[i].tid + '" class="btn btn-info btn-xs riduciBottoneCellulare">Pubblica</button></div></div></div><br/>');
								}
							}
						});

					});
				} else {
					$('#topic_schedulati').remove();
				}

			});

		});

		function formattaData(timestamp) {
			var date = new Date(Number(timestamp));

			// Minutes part from the timestamp
			var mm = date.getMonth() + 1; // getMonth() is zero-based
			var dd = date.getDate();
			var minutes = date.getMinutes();
			var hh = date.getHours();
			var mese = "";
			var day = "";
			var ore = "";
			var min = "";

			if (minutes < 10) {
				min = "0" + minutes;
			} else {
				min = "" + minutes;
			}
			if (mm < 10) {
				mese = "0" + mm;
			} else {
				mese = "" + mm;
			}
			if (dd < 10) {
				day = "0" + dd;
			} else {
				day = "" + dd;
			}
			if (hh < 10) {
				ore = "0" + hh;
			} else {
				ore = "" + hh;
			}
			
			var annoStringa=String(date.getFullYear());
			var anno=annoStringa.substring(2,4);
			var formattedTime = ore + ':' + min;
			var data = day + "/" + mese + "/" +anno;

			return data + " " + formattedTime;
		}

	});
}());