<div>


	<blockquote>Questo plugin permette di schedulare un topic in futuro solo per tutti
	quegli utenti che fanno parte di certi gruppi. In particolare, selezionando la checkbox, è possibile attivare, per gli utenti appartenenti a quel gruppo, la possibilità di schedulare un topic.
	</blockquote>

	<br/>

  
  	<div id="tabella_div" class="table">
      <div class="row">
      <div class="col-xs-12 col-md-9 ">
        <div class="col-xs-5 col-md-5"><label>Nome Gruppo</label></div>
        <div class="col-xs-7 col-md-4 text-center"><label>Deseleziona per rimuovere</label></div>
      </div>
      </div>
      <br/>
      <!-- BEGIN gruppi -->
      <div class="row">
      <div class="col-xs-12 col-md-9">
        <div class="col-xs-5 col-md-5"><label>{gruppi.nome_gruppo}</label></div>
        <!-- IF gruppi.checked -->
          <div class="col-xs-7 col-md-4 text-center"><input type="checkbox" id="{gruppi.nome_gruppo}" checked ></div>
        <!-- ELSE -->
          <div class="col-xs-7 col-md-4 text-center"><input type="checkbox" id="{gruppi.nome_gruppo}" ></div>
        <!-- ENDIF gruppi.checked -->
        </div> 
      </div>
      <!-- END gruppi -->



  		
  	</div>
  
  	<button id="item_save" data-action="save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
        <i class="material-icons">save</i>
       </button>

		
	

</div>
