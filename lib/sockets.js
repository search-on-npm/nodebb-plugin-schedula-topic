'use strict';

var db = module.parent.parent.require('./src/database'),
	winston = require.main.require('winston'),
	websockets = module.parent.parent.require('./src/socket.io'),
	async = module.parent.parent.require('async'),
	cron = require('node-cron'),
	groups = require.main.require('./src/groups'),
	topics = module.parent.parent.require('./topics'),
	categories = module.parent.parent.require('./categories'),
	posts = module.parent.parent.require('./posts'),
	user = module.parent.parent.require('./user'),
	//library = require("../../nodebb-plugin-connect-hashtag/library.js"),
	librarySchedulaTopic = require("../library.js");

var hash = "connect-schedula-topic";

var start = 0;
var stop = -1;
var Sockets = {};


//Salva i gruppi selezioni in admin
Sockets.inserireGruppi = function(socket, data, callback) {

	async.waterfall([
		function(continua) {
			db.delete(hash, continua);
		},
		function(continua) {
			async.each(data.gruppi, function(nome_gruppo, next) {

				async.waterfall([

					function(prossima) {
						db.sortedSetAdd(hash, Date.now(), nome_gruppo, function(err) {
							if (err) {
								return callback(err);
							}
							next();
						});
					}
				], function(err) {
					if (err) {
						return callback(err);
					}
					next();
				});

			}, function(err) {
				if (err) {
					return callback(err);
				}
				continua();
			});
		}
	], function(err) {
		if (err) {
			return callback(err);
		}
		return callback(null, true);
	});


};
//Verifica se un utente appartiene o meno a un gruppo
Sockets.utenteAppartieneGruppi = function(socket, data, callback) {
	if (!data || !data.uid) {
		return callback(new Error("Parametri errati"));
	}
	var uid = data.uid;

	async.waterfall([
		function(next) {
			db.getSortedSetRange(hash, start, stop, next);
		},
		function(nome_gruppi, next) {
			async.each(nome_gruppi, function(i, prossima) {
				groups.isMember(uid, i, function(errore, membro) {
					if (errore) {
						return callback(err);
					}
					if (membro) {
						winston.verbose('[nodebb-connect-schedula-topic]Utente ' + uid + ' appartiene al gruppo ' + i);
						return callback(null, true);
					} else {
						prossima();
					}

				});

			}, function(err) {
				if (err) {
					return callback(err);
				}
				next();
			});
		}
	], function(err) {
		if (err) {
			return callback(err);
		}
		winston.verbose("[nodebb-connect-schedula-topic]L'Utente " + uid + " non appartiene a nessun gruppo");
		return callback(null, false);

	});


};
//Permette di salvare il giorno e la data che è stata inserita
//da un utente
Sockets.salvaUtenteTimestamp = function(socket, data, callback) {

	if (!data || !data.uid || !data.timestamp) {
		return callback(new Error("Parametri Errati"));
	}
	var uid = data.uid;
	var timestamp = data.timestamp;
	var tid = data.tid;
	var creaFunzione;
	var parametri;
	var topicDataVariabile;
	//Il problema sta nel fatto che quando inserisco la data il topic
	//non è stato ancora creato	

	async.waterfall([

		function(next) {
			db.getObject(hash + ":topic_schedulato:" + tid, next);
		},
		function(topicSchedulati, next) {
			if (!topicSchedulati) {
				return next();
			}
			var topic_schedulato = topicSchedulati.topic_schedulato;
			topic_schedulato.timestamp = timestamp;
			db.setObject(hash + ":topic_schedulato:" + tid, {
				topic_schedulato: topic_schedulato
			}, next);
		},
		function(next) {
			winston.verbose("[nodebb-connect-schedula-topic]Topic " + tid + " schedulato dall'utente " + uid + " al timestamp " + timestamp);
			db.setObject(hash + ":uid:" + uid + ":tid:" + tid, {
				'timestamp': timestamp
			}, next);

		},
		function(next) {
			db.getObject(hash + ":funzione_dati:" + tid, next);
		},
		function(topicDataFunzione, next) {
			if (!topicDataFunzione) {
				return next();
			}
			//next();
			topicDataVariabile = topicDataFunzione.parametri;
			topicDataFunzione.parametri.data.timestamp = timestamp;
			topicDataFunzione.parametri.topic.timestamp = timestamp;
			topicDataFunzione.parametri.data.tempoSchedulazione = timestamp;
			db.setObject(hash + ":funzione_dati:" + tid, {
				parametri: topicDataFunzione.parametri
			}, next);

		},

		function(next) {
			var lista_timer = {};
			if (librarySchedulaTopic && librarySchedulaTopic.allTimer) {
				lista_timer = librarySchedulaTopic.allTimer;
				var timer_task = lista_timer[tid].task;
				creaFunzione = lista_timer[tid].funzione;
				parametri = lista_timer[tid].parametri;
				if (timer_task) {
					timer_task.destroy();
					delete lista_timer[tid];
				}
			}


			var date = new Date(Number(timestamp));


			var mese = date.getMonth() + 1; // getMonth() is zero-based
			var giorno = date.getDate();
			var minuti = date.getMinutes();
			var ore = date.getHours();
			var anno = date.getFullYear();


			var stringa = '0 ' + minuti + ' ' + (ore) + ' ' + giorno + ' ' + mese + ' *';
			var task = cron.schedule(stringa, function() {
				async.waterfall([

					function(avanti) {
						db.sortedSetRemove(hash + ":timer", tid, avanti);
					},
					function(avanti) {
						db.delete(hash + ":timer:tid:" + tid, avanti);
					},
					function(avanti) {
						db.delete(hash + ":uid:" + uid + ":" + tid, avanti);
					},
					function(avanti) {
						db.sortedSetRemove(hash + ":uid:" + uid, tid, avanti);
					},
					function(avanti) {
						db.delete(hash + ":topic_schedulato:" + tid, avanti);
					},
					function(avanti) {
						db.delete(hash + ":funzione_dati:" + tid, avanti);
					},
					function(avanti) {
						var param = {
							titolo: topicDataVariabile.topic.title,
							indirizzo: topicDataVariabile.topic.slug
						};
						websockets.in('uid_' + uid).emit('connectschedulatopic_schedulato', {
							parametri: param
						});
						avanti();

					}

				], function(err) {
					if (err) {
						return callback(err);
					}
					creaFunzione(null, parametri);
					task.destroy();
					delete lista_timer[tid];

					//return callback(null, data);
				});

			}, false);
			lista_timer[tid] = {
				task: task,
				funzione: creaFunzione,
				parametri: parametri
			};

			task.start();
			next();
		}
	], function(err) {
		if (err) {
			return callback(err);
		}
		return callback(null, data);
	});

};


//Ritorna i topic che attualmente sono ancora schedulati in futuro
Sockets.getTopicsSchedulati = function(socket, data, callback) {

	if (!data || !data.uid) {
		return callback(new Error("ERRORE"));
	}
	var uid = data.uid;
	var array_finale = [];
	async.waterfall([
		function(next) {
			db.getSortedSetRange(hash + ":uid:" + uid, start, stop, next);
		},
		function(tids, next) {
			if (!tids) {
				return callback();
			}
			async.each(tids, function(tid, prossima) {
				async.waterfall([
					function(avanti) {
						db.getObject(hash + ":topic_schedulato:" + tid, avanti);
						//topics.getTopicData(tid, avanti);
					},
					function(topic, avanti) {
						if (!topic || !topic.topic_schedulato || !topic.topic_schedulato.timestamp) {

							return prossima();
						}
						var topic_schedulato = topic.topic_schedulato;

						array_finale.push(topic_schedulato);
						return avanti();
					}
				], function(err) {
					if (err) {
						return callback(err);
					}

					return prossima();

				});
			}, function(err) {
				if (err) {
					return callback(err);
				}

				return next();
			});
		}
	], function(err) {
		if (err) {
			return callback(err);
		}

		//array_finale = array_finale.reverse();
		return callback(null, array_finale);
	});



};
//Elimina un topic schedulato e per farlo devo creare un cron
//che permetta di schedulare il topic
Sockets.eliminaTopicSchedulato = function(socket, data, callback) {
	if (!data || !data.tid || !data.uid) {
		return callback(new Error("Parametri Errati"));
	}

	var tid = data.tid;
	var uid = data.uid;
	var lista_timer = {};
	var creaFunzione;
	var parametri;

	if (librarySchedulaTopic && librarySchedulaTopic.allTimer) {
		lista_timer = librarySchedulaTopic.allTimer;
		var timer_task = lista_timer[tid].task;
		creaFunzione = lista_timer[tid].funzione;
		parametri = lista_timer[tid].parametri;
		if (timer_task) {
			timer_task.destroy();
			delete lista_timer[tid];
		}
		var timestamp = Date.now();
		parametri.data.timestamp = timestamp;
		parametri.data.tempoSchedulazione = timestamp;
		parametri.topic.timestamp = timestamp;
		//parametri.data.timestamp = parametri.data.tempoSchedulazione;
	}

	async.waterfall([

		function(avanti) {
			db.sortedSetRemove(hash + ":timer", tid, avanti);
		},
		function(avanti) {
			db.delete(hash + ":timer:tid:" + tid, avanti);
		},
		function(avanti) {
			db.delete(hash + ":uid:" + uid + ":" + tid, avanti);
		},
		function(avanti) {
			db.sortedSetRemove(hash + ":uid:" + uid, tid, avanti);
		},
		function(avanti) {
			db.delete(hash + ":topic_schedulato:" + tid, avanti);
		},
		function(avanti) {
			db.delete(hash + ":funzione_dati:" + tid, avanti);
		},
		function(avanti) {
			var param = {
				titolo: parametri.topic.title,
				indirizzo: parametri.topic.slug
			};
			websockets.in('uid_' + uid).emit('connectschedulatopic_pubblica', {
				parametri: param
			});
			avanti();

		}

	], function(err) {
		if (err) {
			return callback(err);
		}
		creaFunzione(null, parametri);
		return callback(null, data);


	});

};


Sockets.getTopicPid = function(socket, data, callback) {
	if (!data || !data.pid) {
		return callback(new Error("Parametri Errati"));
	}
	var pid = data.pid;

	async.waterfall([
		function(next) {
			posts.getPostData(pid, next);
		},
		function(post, next) {
			topics.getTopicData(post.tid, next);
		}
	], function(err, topic) {
		if (err) {
			return callback(err);
		}
		return callback(null, topic);
	});
};
//Ritorna il topic schedulato
Sockets.getTopicSchedulato = function(socket, data, callback) {
	if (!data || !data.tid) {
		return callback(new Error("Parametri errati"));

	}
	var tid = data.tid;
	db.getObject(hash + ":topic_schedulato:" + tid, function(err, topicData) {
		if (err) {
			return callback(err);
		}
		if (!topicData) {
			return callback();
		}
		return callback(null, topicData.topic_schedulato);
	});
};



module.exports = Sockets;