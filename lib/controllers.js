'use strict';

var db = require.main.require('./src/database'),
	async = require.main.require('async'),
	groups = require.main.require('./src/groups'),

	Controllers = {};

//var set = 'groups:visible:createtime';
var set = 'groups:createtime';
var hash = "connect-schedula-topic";
var start = 0,
	stop = -1;


Controllers.renderAdmin = function(req, res, callback) {
	async.parallel({
			'gruppi': function(next) {
				async.waterfall([
					function(prima) {
						db.getSortedSetRange('groups:createtime', 0, -1, prima);
					},
					function(groupNames, prima) {

						groupNames = groupNames.filter(function(name) {
							return !groups.isPrivilegeGroup(name);
						});

						groups.getGroupsData(groupNames, prima);

					}

				], function(err, gruppi) {
					if (err) {
						return callback(err);
					}
					var gruppi_finali = [];
					//Ordino per nome dei gruppi
					gruppi = gruppi.sort(function(a, b) {
						a = a.name.toLowerCase();
						b = b.name.toLowerCase();
						if (a < b) {
							return -1;

						}
						if (a > b) {
							return 1;
						}
						return 0;
					});
					//var arr=[];

					/*for (var i = 0; i < gruppi.length; i++) {
						var nome_gruppo = gruppi[i];								
							gruppi_finali.push({
								'nome_gruppo': gruppi[i].name
							});								
						
					}*/
					return next(null, gruppi);
				});


			},
			'gruppi_selezionati': function(next) {

				db.getSortedSetRange(hash, start, stop, next);
			}
		},
		function(err, risultato) {
			if (err) {
				return callback(err);
			}
			var oggetto_finale = [];
			for (var i = 0; i < risultato.gruppi.length; i++) {
				if (isContains(risultato.gruppi[i].name, risultato.gruppi_selezionati)) {
					oggetto_finale.push({
						'nome_gruppo': risultato.gruppi[i].name,
						'checked': true
					});
				} else {
					oggetto_finale.push({
						'nome_gruppo': risultato.gruppi[i].name,
						'checked': false
					});
				}
			}
			res.render('admin/plugins/connect-schedula-topic', {
				'gruppi': oggetto_finale

			});

		});

};

function isContains(nome_gruppo, gruppi_cheched) {
	for (var i = 0; i < gruppi_cheched.length; i++) {
		if (gruppi_cheched[i] == nome_gruppo) {
			return true;
		}
	}
	return false;
}

module.exports = Controllers;