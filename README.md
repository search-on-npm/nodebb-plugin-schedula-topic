# Schedula-Topic


<br>
Questo plugin permette agli utenti che appartengono a un determinato gruppo, di pubblicare un topic nel futuro.
<br>



**[LATO ADMIN]**<br/>

Lato admin è possibile selezionare i gruppi che sono stati creati all'interno del Forum:<br/>


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-schedula-topic/raw/demaoui/screenshot/connect-schedula-topic-admin.png)<br />


Una volta selezioni i gruppi che possono schedulare un topic è possibile salvare.<br/>

**[LATO CLIENT]**<br/><br/>
Un utente, che appartiene necessariamente a uno dei gruppi selezionati dall'admin, quando andrà a creare un topic, l'edit comparirà in questa maniera:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-schedula-topic/raw/demaoui/screenshot/connect-schedula-topic-editor1.png)<br/><br/>

Cliccando sull'iconcina evidenziata dal cerchio nero si aprirà una finestra in cui è possibile selezionare la data e l'ora a cui deve essere schedulato il topic:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-schedula-topic/raw/demaoui/screenshot/connect-schedula-topic-editor2.png)<br/><br/>

Una volta selezionata la data salvara attraverso l'apposito bottone. Dopo aver fatto ciò è possibile inserire il testo del topic è salvare. In questo modo il topic non sarà visibile fino a quando non arriverà il giorno e l'ora inserita dall'utente precedentemente. Una volta creato il topic, esso comparirà in questa maniera:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-schedula-topic/raw/demaoui/screenshot/connect-schedulata-topic-creato.png)<br/><br/>

Tutti i normali pulsanti sono stati eliminati l'unica cosa che è permessa fare sono:<br/>
* **Edit**  permette la modifica del topic, della data etc.<br/>
* **Cancella Topic**  permette la cancellazione del topic (cancellerà anche la data prima inserita)<br/>
<br/>
Una volta che un topic è stato schedulato comparirà, nella pagina dell'utente, un bottone che permetterà la visualizzazione di tutti i topic schedulati in precedemza:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-schedula-topic/raw/demaoui/screenshot/connect-schedula-topic-page1.png)<br/><br/>

Premendo quel bottone si aprirà una finestra in cui ci sarà una tabella:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-schedula-topic/raw/demaoui/screenshot/connect-schedula-topic-page2.png)<br/><br/>


Questa tabella conterrà:<br/>
* **Nome topic** il nome del topic<br/>
* **Data Schedulazione** la data secondo la quale il topic sarà schedulato<br/>
* **Modifica** permette di modificare il topic<br/>
* **Pubblica ora** permette di pubblicare il topic subito<br/>

<br>/
Quando si cliccherà su "modifica" si aprirà  la pagina dell'edit che permetterà di modificare il topic mentre quando si clicca su "Pubblica Ora" il topic verrà pubblicato subito eliminando la data e il tempo precedentemente utilizzati.<br/>


**[TEST]**<br/><br/>
Per testare se effettivamente i topic sono schedulati e sono in ordine con i topic che non sono stati schedulati bisogna controllare a:<br/>
* localhost/user/admin/topics
* localhost/recent
* localhost/user/admin
































